$ErrorActionPreference = 'Stop'

$si = New-Object System.Diagnostics.ProcessStartInfo
$si.FileName = $args[0]
$si.Arguments = [String]::Join(' ', $args[1..($args.Count - 1)])
$si.Verb = 'RunAs'
$si.UseShellExecute = $true

$process = [System.Diagnostics.Process]::Start($si)

# Very strange code...
# But I spy it in MSBuild...
# I hope these guys know what they are doing! :)

$process.WaitForExit()

do
{
    [System.Threading.Thread]::Sleep(0)
}
while (!$process.HasExited)

#Exit $process.ExitCode