@echo off
setlocal enableextensions enabledelayedexpansion

SET vpn_connect=VPN-Kosmos-6
SET vpn_login=vibro
SET vpn_password=V1br0-vpn

:: IP и маска сети куда нужно попасть
SET int_pptp=192.168.43.0
SET int_mask=255.255.0.0

:: Часть IP адреса которое получает VPN соединение. Без последней цифры и с точкой.
SET ext_pptp=192.168.43.

:: Устанавливаем VPN соединение
rasdial %vpn_connect% %vpn_login% %vpn_password%

:: Находим IP адрес который присваивается VPN клиенту
for /F "tokens=1,2 delims=:" %%i in ('ipconfig^|findstr %ext_pptp%') do (
	
	::И добавляем новый маршрут. Шлюзом будет наш найденный IP
	sudo route add %int_pptp% mask %int_mask%%%j
)

pause
