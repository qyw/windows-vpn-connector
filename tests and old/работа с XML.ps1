﻿# VPN Connection provisioning 
$vpn_name = "VPN-Kosmos6"
$vpn_server = "sledge.triolan.com.ua"
$target_nets =  "192.168.99.0/24, 192.168.43.0/24"

[xml]$task_xml = Get-Content $PSScriptRoot\vpn-conn-update-task.xml
$task_xml.Task.RegistrationInfo.URI = "\$vpn_name add routes on connect"
$task_xml.Task.Triggers.EventTrigger.Subscription = '<QueryList><Query Id="0" Path="Application"><Select Path="Application">*[System[(Level=4 or Level=0) and (EventID=20225)]] and *[EventData[Data="'+$vpn_name+'"]]</Select></Query></QueryList>'
$task_xml.Task.Actions.Exec.Command = "Powershell.exe"
$task_xml.Task.Actions.Exec.Arguments = "-noProfile -NonInteractive `".\vpn-route.ps1 VPN-Kosmos6 -TargetNets 192.168.99.0/24, 192.168.43.0/24`""
$task_xml.Task.Actions.Exec.WorkingDirectory = $PSScriptRoot
$task_xml.Save("$PSScriptRoot\vpn-conn-update-task-1111.xml")

