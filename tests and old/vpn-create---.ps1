# VPN Connection provisioning 
$vpn_name = "VPN-Kosmos6"
$vpn_server = "sledge.triolan.com.ua"
$vpn_login = "vibro"
$vpn_password = "V1br0-vpn"

# IP � ����� ���� ���� ����� �������
$int_vpn_addr = "192.168.43.0"
$int_vpn_mask = "255.255.0.0"


#------------------------------------------------------------
# VPN Connection look-up to check any previous installations
#------------------------------------------------------------
$isTestVpn = $false
$vpnConnections = Get-VpnConnection #-AllUserConnection

if($vpnConnections.Name -eq $vpn_name)
{
    Write-Host $vpn_name connection is already configured on your system. -ForegroundColor Yellow
    Write-Host "If you wish to reinstall, please uninstall the connection and then attemp again." -ForegroundColor Yellow
    #Write-Host ""
    #Write-Host "Press any key to exit..."
    $x = read-host "Do you wish to uninstall $vpn_name (Y or N)"
	#while( $x -neq "Y" -or $x -neq "n" )
	if( $x[0] -eq "Y" ){ rasdial $vpn_name /disconnect; Remove-VpnConnection -Name $vpn_name -Force  }
	else { exit }
}

Write-Host Installing $vpn_name connection.

try
{
    # Create the VPN connection �My VPN� with the EAP configuration XML generated above
    Add-VpnConnection -Name $vpn_name -ServerAddress $vpn_server -EncryptionLevel Required -TunnelType PPTP #-AuthenticationMethod MSChapV2 -SplitTunneling $True
	Set-VpnConnection -Name $vpn_name -SplitTunneling $True -RememberCredential $True
}
catch
{
    Write-Host "Error in connection setup!" -ForegroundColor Red -BackgroundColor Black
    Write-Host $_.Exception.Message
    throw
}

#Write-Host ""
Write-Host $vpn_name connection is ready for use.


#------------------------------------------------------------
# Schedule task on VPN connection established
#------------------------------------------------------------
$vpnRouteScriptPath = $PSScriptRoot"vpn-route.ps1"
./sudo.ps1 schtasks /create /F /TN "$vpn_name Connection Update" /TR "Powershell.exe -NonInteractive -noexit -command $vpnRouteScriptPath" /SC ONEVENT /EC Application /MO "*[System[(Level=4 or Level=0) and (EventID=20225)]] and *[EventData[Data='$vpn_name']]" /RL HIGHEST #/RU �������������

#schtasks /create /F /TN "VPN Connection Update" /TR "Powershell.exe -NonInteractive -command \\vpnserver.contoso.com\scripts\create-conn.ps1 \\vpnserver.contoso.com\scripts\settings.xml" /SC ONEVENT /EC Application /MO " *[System[(Level=4 or Level=0) and (EventID=20225)]] and *[EventData[Data='Contoso VPN']] "
Write-Host "Add route task scheduled."


Write-Host "Press any key to exit..."
$x = read-host
