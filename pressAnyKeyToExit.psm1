function pressAnyKeyToExit {
	write-host "Press any key to exit... "
	$null = $host.ui.RawUI.ReadKey("NoEcho, IncludeKeyDown")
}
